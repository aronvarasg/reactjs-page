import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import ErrorPage from "./layout/ErrorPage";
import Home from "./components/Home";

function App() {
  return (
    <Router>
      <Routes>
        <Route index path='/' element={<Home />}></Route>
        <Route path='*' element={<ErrorPage />}></Route>
      </Routes>
    </Router>
  );
}

export default App;
