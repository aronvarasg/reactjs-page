const HeaderImages = {
    Banner: require('./header.jpg'),
    Logo: require('./Leaf_Team.png'),
    Hexagon: require('./hexagon-fade-down.png')
}

export default HeaderImages;