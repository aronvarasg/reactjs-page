# React ⚛

[![Gitlab License](https://img.shields.io/badge/Repo-GitLab-orange.svg)](https://gitlab.com/aronvarasg/reactjs-page/)
[![ReactJs References](https://img.shields.io/badge/Reference-ReactJs-yellow.svg)](https://reactjs.org/docs/getting-started.html/)
[![NodeJs References](https://img.shields.io/badge/Reference-NodeJs-green.svg)](https://nodejs.org/en/docs/)

### Pagina personal Aaron Varas

## Componentes Utilizados 🚀

* Visual Studio Code
* NodeJs
* ReactJs
* BootStrap

## Instalacion de ambiente 🏘

```
npx create-react-app my-app
cd my-app
npm start
```
